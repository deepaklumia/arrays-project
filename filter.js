function filter(elements, cb) {
      if (elements !== undefined || cb !== undefined) {
            let odd = [];
            for (let index = 0; index < elements.length; index++) {
                  let check = cb(elements[index], index, elements);
                  if (check) {
                        odd.push(elements[index]);
                  }
            }
            return odd;
      }
      else {
            return [];
      }
}
module.exports = filter;
