//let depth = 6;
function flatten(elements, depth) {
  if (depth == null) {
    return flattenAlways(elements, [])
  } else {
    return flattenDepth(elements, [], depth)
  }
  function flattenAlways(elements, result) {
    for (let index = 0; index < elements.length; index++) {
      let value = elements[index]

      if (Array.isArray(value)) {
        flattenAlways(value, result)
      } else if (value === undefined) {
        continue;
      }
      else {
        result.push(value)
      }
    }

    return result
  }
  function flattenDepth(elements, result, depth) {
    for (let index = 0; index < elements.length; index++) {
      let value = elements[index]

      if (depth > 0 && Array.isArray(value)) {
        flattenDepth(value, result, depth - 1)
      } else if (value === undefined) {
        continue;
      }
      else {
        result.push(value)
      }
    }

    return result
  }

}
module.exports = flatten;
